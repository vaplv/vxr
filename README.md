# VoXel Renderer

This C89 code base implements a simple renderer relying onto the `VoXel
Library` (VxL) engine. It only provides renderer specific data structure like
the camera or the framebuffer; the geometry must be explicitly managed through
the `VxL` library. Note that currently the renderer only compute basic
ray-casting; neither advanced lighting nor material shading is implemented,
  yet.

## How to build

The library uses [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also
depends on the [RSys](https://gitlab.com/vaplv/rsys/) and the
[VxL](https://gitlab.com/vaplv/vxl/) libraries. First, install the
RCMake package as well as the RSys and VxL libraries. Then, generate the
project from the cmake/CMakeLists.txt file by appending the RCMake, RSys and
VxL install directories to the `CMAKE_PREFIX_PATH` variable. The resulting
project can finally be edited, built, tested and installed as any CMake
project.

## License

VxR is Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr). It is a free
software released under the [OSI](https://opensource.org)-approved GPL v3+
license. You are welcome to redistribute it under certain conditions; refer to
the COPYING file for details.

