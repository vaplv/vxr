/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXR_TILE_H
#define VXR_TILE_H

#include <rsys/dynamic_array_char.h>
#include <rsys/dynamic_array_float.h>

#include <vx/vxl.h>

#define DARRAY_NAME hit
#define DARRAY_DATA struct vxl_hit
#include <rsys/dynamic_array.h>

struct tile {
  struct darray_char pixels; /* List of UByte RGBA pixels */
  struct darray_float dirs; /* List of float[3] ray dirs */
  struct darray_float dirs_dx; /* List of float[3] differential dirs in x */
  struct darray_float dirs_dy; /* List of float[3] differential dirs in y */
  struct darray_float ranges; /* List of float[2] ray ranges */
  struct darray_hit hits;
};

static FINLINE void
tile_init(struct mem_allocator* allocator, struct tile* tile)
{
  ASSERT(tile);
  darray_char_init(allocator, &tile->pixels);
  darray_float_init(allocator, &tile->dirs);
  darray_float_init(allocator, &tile->dirs_dx);
  darray_float_init(allocator, &tile->dirs_dy);
  darray_float_init(allocator, &tile->ranges);
  darray_hit_init(allocator, &tile->hits);
}

static FINLINE void
tile_release(struct tile* tile)
{
  ASSERT(tile);
  darray_char_release(&tile->pixels);
  darray_float_release(&tile->dirs);
  darray_float_release(&tile->dirs_dx);
  darray_float_release(&tile->dirs_dy);
  darray_float_release(&tile->ranges);
  darray_hit_release(&tile->hits);
}

#define CALL(Func) {                                                           \
  const res_T res = Func;                                                      \
  if(res != RES_OK) return res;                                                \
} (void)0

static FINLINE res_T
tile_copy(struct tile* dst, const struct tile* src)
{
  ASSERT(dst && src);
  CALL(darray_char_copy(&dst->pixels, &src->pixels));
  CALL(darray_float_copy(&dst->dirs, &src->dirs));
  CALL(darray_float_copy(&dst->dirs_dx, &src->dirs_dx));
  CALL(darray_float_copy(&dst->dirs_dy, &src->dirs_dy));
  CALL(darray_float_copy(&dst->ranges, &src->ranges));
  CALL(darray_hit_copy(&dst->hits, &src->hits));
  return RES_OK;
}

static FINLINE res_T
tile_copy_and_release(struct tile* dst, struct tile* src)
{
  ASSERT(dst && src);
  CALL(darray_char_copy_and_release(&dst->pixels, &src->pixels));
  CALL(darray_float_copy_and_release(&dst->dirs, &src->dirs));
  CALL(darray_float_copy_and_release(&dst->dirs_dx, &src->dirs_dx));
  CALL(darray_float_copy_and_release(&dst->dirs_dy, &src->dirs_dy));
  CALL(darray_float_copy_and_release(&dst->ranges, &src->ranges));
  CALL(darray_hit_copy_and_release(&dst->hits, &src->hits));
  return RES_OK;
}


static FINLINE res_T
tile_resize(struct tile* tile, const uint16_t width, const uint16_t height)
{
  const size_t npixels = (size_t)(width * height);
  ASSERT(tile);
  CALL(darray_char_resize(&tile->pixels, npixels * 4/*RGBA*/));
  CALL(darray_float_resize(&tile->dirs, npixels * 3/*XYZ*/));
  CALL(darray_float_resize(&tile->dirs_dx, npixels * 3/*XYZ*/));
  CALL(darray_float_resize(&tile->dirs_dy, npixels * 3/*XYZ*/));
  CALL(darray_float_resize(&tile->ranges, npixels * 2/*Min/Max*/));
  CALL(darray_hit_resize(&tile->hits, npixels));
  return RES_OK;
}
#undef CALL

static FINLINE char*
tile_get_pixels(struct tile* tile)
{
  ASSERT(tile);
  return darray_char_data_get(&tile->pixels);
}

static FINLINE float*
tile_get_directions(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->dirs);
}

static FINLINE float*
tile_get_directions_dx(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->dirs_dx);
}

static FINLINE float*
tile_get_directions_dy(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->dirs_dy);
}

static FINLINE float*
tile_get_ranges(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->ranges);
}

static FINLINE struct vxl_hit*
tile_get_hits(struct tile* tile)
{
  ASSERT(tile);
  return darray_hit_data_get(&tile->hits);
}

#endif /* VXR_TILE_H */

