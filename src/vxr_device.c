/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxr_c.h"
#include "vxr_device.h"

#include <rsys/mem_allocator.h>
#include <vx/vxl.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
device_release(ref_T* ref)
{
  struct vxr_device* dev = CONTAINER_OF(ref, struct vxr_device, ref);
  ASSERT(ref);
  if(dev->vxl_sys)
    VXL(system_ref_put(dev->vxl_sys));
  darray_tile_release(&dev->tiles);
  MEM_RM(dev->allocator, dev);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
VXR_DECLARE_REF_FUNCS(device)

res_T
vxr_device_create
  (struct mem_allocator* allocator,
   struct vxl_system* vxl_sys,
   const unsigned nthreads_hint,
   struct vxr_device** dev_out)
{
  struct vxr_device* dev = NULL;
  struct mem_allocator* mem_allocator;
  unsigned vxl_nthreads;
  res_T res = RES_OK;

  if(!dev_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  mem_allocator = allocator ? allocator : &mem_default_allocator;
  dev = MEM_CALLOC(mem_allocator, 1, sizeof(struct vxr_device));
  if(!dev) {
    res = RES_MEM_ERR;
    goto error;
  }
  dev->allocator = mem_allocator;
  ref_init(&dev->ref);
  darray_tile_init(dev->allocator, &dev->tiles);
  res = darray_tile_resize(&dev->tiles, 1);
  if(res != RES_OK) goto error;

  if(!vxl_sys) {
    res = vxl_system_create(dev->allocator, 1.f, &vxl_sys);
  } else {
    res = vxl_system_ref_get(vxl_sys);
  }
  if(res != RES_OK)
    goto error;

  dev->vxl_sys = vxl_sys;
  res = vxl_system_get_nthreads(vxl_sys, &vxl_nthreads);
  if(res != RES_OK) goto error;

  dev->nthreads = MMIN(nthreads_hint, vxl_nthreads);
  res = darray_tile_resize(&dev->tiles, dev->nthreads);
  if(res != RES_OK) goto error;

exit:
  if(dev_out) *dev_out = dev;
  return res;
error:
  if(dev) {
    VXR(device_ref_put(dev));
    dev = NULL;
  }
  goto exit;
}

res_T
vxr_device_get_system(struct vxr_device* dev, struct vxl_system** sys)
{
  if(!dev || !sys) return RES_BAD_ARG;
  *sys = dev->vxl_sys;
  return RES_OK;
}

