/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxr_c.h"
#include "vxr_device.h"

#include <string.h>

struct vxr_render_buffer {
  struct vxr_image img;
  ref_T ref;
  struct vxr_device* dev;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
render_buffer_release(ref_T* ref)
{
  struct vxr_render_buffer* buf = CONTAINER_OF
    (ref, struct vxr_render_buffer, ref);
  struct vxr_device* dev;
  ASSERT(ref);
  dev = buf->dev;
  if(buf->img.data)
    MEM_RM(dev->allocator, buf->img.data);
  MEM_RM(dev->allocator, buf);
  VXR(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
VXR_DECLARE_REF_FUNCS(render_buffer)

res_T
vxr_render_buffer_create
  (struct vxr_device* dev,
   struct vxr_render_buffer** buf_out)
{
  struct vxr_render_buffer* buf = NULL;
  res_T res = RES_OK;

  if(!dev || !buf_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  buf = MEM_CALLOC(dev->allocator, 1, sizeof(struct vxr_render_buffer));
  if(!buf) {
    res = RES_MEM_ERR;
    goto error;
  }
  VXR(device_ref_get(dev));
  buf->dev = dev;
  ref_init(&buf->ref);

exit:
  if(buf_out)
    *buf_out = buf;
  return res;
error:
  if(buf) {
    VXR(render_buffer_ref_put(buf));
    buf = NULL;
  }
  goto exit;
}

res_T
vxr_render_buffer_setup
  (struct vxr_render_buffer* buf,
   const unsigned size[2],
   const enum vxr_pixel_format format)
{
  size_t pitch;
  void* data;

  if(!buf || !size || !size[0] || !size[1])
    return RES_BAD_ARG;

  pitch = size[0] * VXR_SIZEOF_PIXEL_FORMAT(format);
  data = MEM_ALLOC_ALIGNED(buf->dev->allocator, pitch * size[1], 16);
  if(!data)
    return RES_MEM_ERR;

  if(buf->img.data) {
    MEM_RM(buf->dev->allocator, buf->img.data);
    buf->img.data = NULL;
    buf->img.pitch = 0;
    buf->img.size[0] = buf->img.size[1] = 0;
  }
  buf->img.data = data;
  buf->img.pitch = pitch;
  buf->img.size[0] = size[0];
  buf->img.size[1] = size[1];
  buf->img.format = format;
  return RES_OK;
}

res_T
vxr_render_buffer_image_get
  (struct vxr_render_buffer* buf, struct vxr_image* img)
{
  if(!buf || !img)
    return RES_BAD_ARG;
  *img = buf->img;
  return RES_OK;
}

res_T
vxr_render_buffer_write
  (void* context,
   const unsigned origin[2],
   const unsigned size[2],
   const enum vxr_pixel_format format,
   const void* pixels)
{
  struct vxr_render_buffer* rbuf = context;
  const char* src_row = pixels;
  size_t BPP;
  size_t iy;
  size_t tile_pitch, itile_x;

  if(UNLIKELY(!context || !origin || !size || !pixels))
    return RES_BAD_ARG;
  if(UNLIKELY(format != rbuf->img.format || !rbuf->img.data))
    return RES_BAD_ARG;

  if(UNLIKELY((origin[0] + size[0]) > rbuf->img.size[0]))
    return RES_BAD_ARG;
  if(UNLIKELY(origin[1] + size[1] > rbuf->img.size[1]))
    return RES_BAD_ARG;

  BPP = VXR_SIZEOF_PIXEL_FORMAT(rbuf->img.format);
  tile_pitch = size[0] * BPP;
  itile_x = origin[0] * BPP;

  FOR_EACH(iy, origin[1], origin[1] + size[1]) {
    const size_t itile_row = iy * rbuf->img.pitch + itile_x;
    memcpy(rbuf->img.data + itile_row, src_row, tile_pitch);
    src_row += tile_pitch;
  }
  return RES_OK;
}

