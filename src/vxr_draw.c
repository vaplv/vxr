/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxr.h"
#include "vxr_camera.h"
#include "vxr_device.h"
#include "vxr_framebuffer.h"

#include <rsys/float2.h>
#include <vx/vxl.h>

#include <omp.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static FINLINE uint16_t
morton2D_decode(const uint32_t u32)
{
  uint32_t x = u32 & 0x55555555;
  x = (x | (x >> 1)) & 0x33333333;
  x = (x | (x >> 2)) & 0x0F0F0F0F;
  x = (x | (x >> 4)) & 0x00FF00FF;
  x = (x | (x >> 8)) & 0x0000FFFF;
  return (uint16_t)x;
}

static FINLINE uint32_t
morton2D_encode(const uint16_t u16)
{
  uint32_t x = u16;
  x = (x | (x << 8)) & 0x00FF00FF;
  x = (x | (x << 4)) & 0x0F0F0F0F;
  x = (x | (x << 2)) & 0x33333333;
  x = (x | (x << 1)) & 0x55555555;
  return x;
}

static FINLINE void
hit_shade
  (const struct vxl_hit* hit,
   const enum vxr_render_type rtype,
   const float ray_dir[3],
   unsigned char rgba[4])
{
  int i;
  ASSERT(hit && ray_dir && rgba);

  if(VXL_HIT_NONE(hit)) {
    FOR_EACH(i, 0, 3) rgba[i] = 0;
    rgba[3] = 255;
  } else if(rtype == VXR_RENDER_NORMAL) {
    FOR_EACH(i, 0, 3)
      rgba[i] = (unsigned char)(absf(hit->normal[i]) * 255.f);
    rgba[3] = 255;
  } else if(rtype == VXR_RENDER_COLOR) {
    FOR_EACH(i, 0, 3) rgba[i] = hit->color[i];
    rgba[3] = 255;
  } else {
    float cosine, N[3] = {0.f, 0.f, 0.f};
    switch(rtype) {
      case VXR_RENDER_LEGACY: /* Use the normal attrib */
        f3_set(N, hit->normal);
        break;
      case VXR_RENDER_CUBIC: /* Use voxel face normal */
        /* Normals point outward the voxel */
        N[hit->face/2] = hit->face % 2 ? 1.f : -1.f;
        break;
      default: FATAL("Unreachable code\n"); break;
    }
    if(0 > (cosine = f3_dot(N, ray_dir))) {
      cosine = f3_dot(f3_minus(N, N), ray_dir);
    }
    FOR_EACH(i, 0, 3) rgba[i] = (unsigned char)(hit->color[i] * cosine);
    rgba[3] = 255;
  }
}

/* Compute the min beam depth from its reference ray and its neighborhood */
static FINLINE void
beam_compute_min_depth
  (struct tile* tile,
   const uint16_t ibeam_x, /* Index of the beam in X */
   const uint16_t ibeam_y) /* Index of the beam in Y */
{
  struct vxl_hit* hits;
  uint32_t mcode_x[2], mcode_y[2];
  uint32_t mcodes[4];
  uint32_t imin;
  float voxel_diag;
  int i;
  ASSERT(tile);
  hits = tile_get_hits(tile);
  mcode_x[0] = morton2D_encode((uint16_t)(ibeam_x + 0)) << 0;
  mcode_x[1] = morton2D_encode((uint16_t)(ibeam_x + 1)) << 0;
  mcode_y[0] = morton2D_encode((uint16_t)(ibeam_y + 0)) << 1;
  mcode_y[1] = morton2D_encode((uint16_t)(ibeam_y + 1)) << 1;
  mcodes[0] = mcode_x[0] | mcode_y[0];
  mcodes[1] = mcode_x[0] | mcode_y[1];
  mcodes[2] = mcode_x[1] | mcode_y[0];
  mcodes[3] = mcode_x[1] | mcode_y[1];

  imin = mcodes[0];
  FOR_EACH(i, 1, 4) {
    if(hits[mcodes[i]].distance < hits[imin].distance)
      imin = mcodes[i];
  }
  /* Conservatively adjust the min beam depth to ensure that the beam rays
   * start outside the first non empty voxel intersected by the beam. To do
   * this, simply subtract the voxel diagonal half size to the min beam depth */
  voxel_diag = hits[imin].size * 1.73205080756887729352f/*sqrt(3)*/;
  hits[mcodes[0]].distance = MMAX(1.e-6f, hits[imin].distance - voxel_diag*0.5f);
}

static FINLINE float
beam_get_min_depth
  (struct tile* tile,
   const uint16_t ibeam_x, /* Index of the beam in X */
   const uint16_t ibeam_y) /* Index of the beam in Y */
{
  struct vxl_hit* hits;
  uint32_t mcode_x, mcode_y;
  ASSERT(tile);
  hits = tile_get_hits(tile);
  mcode_x = morton2D_encode(ibeam_x) << 0;
  mcode_y = morton2D_encode(ibeam_y) << 1;
  return hits[mcode_x | mcode_y].distance;
}

/* Ray trace the tile beam in morton order. The result is a per beam hit
 * storing the conservative minimum hit distance into the beam. This distance
 * can be safely used as the starting point of the rays to trace into the beam
 * as described in "Efficient Sparse Voxel Octrees" of Samuli Laine and Tero
 * Karras */
static FINLINE void
beam_trace
  (struct tile* tile,
   struct vxl_scene* scene,
   struct vxr_camera* cam,
   struct vxr_framebuffer* fbuf,
   const unsigned tile_origin[2], /* Origin in pixels of the tile */
   const unsigned tile_size[2], /* tile # pixels */
   const unsigned beam_size[2], /* per beam # pixels */
   const struct vxr_draw_desc* state)
{
  float* ray_dirs, *ray_dirs_dx, *ray_dirs_dy, *ray_ranges;
  struct vxl_hit* hits;
  struct vxl_rt_desc rt_state;
  float ray_org[3];
  float pixel_size[2];
  uint32_t beams_count[2];
  uint32_t nbeams;
  uint32_t mcode;
  uint16_t ibeam_x, ibeam_y;
  ASSERT(tile && tile_origin && tile_size && beam_size && state);

  pixel_size[0] = 1.f / (float)fbuf->size[0];
  pixel_size[1] = 1.f / (float)fbuf->size[1];

  /* We ray trace the 4 corners of each tiled beam. Actually, only the upper
   * left corner of a beam is explicitly ray-traced; its other corners are
   * retrieve from its neighborhood. Note that we have to add one to the beams
   * count to ensure that the border beams have valid neighbors */
  beams_count[0] = (tile_size[0] + (beam_size[0] - 1)/*ceil*/)/beam_size[0] + 1;
  beams_count[1] = (tile_size[1] + (beam_size[1] - 1)/*ceil*/)/beam_size[1] + 1;

  ray_dirs = tile_get_directions(tile);
  ray_dirs_dx = tile_get_directions_dx(tile);
  ray_dirs_dy = tile_get_directions_dy(tile);
  ray_ranges = tile_get_ranges(tile);
  hits = tile_get_hits(tile);

  /* Adjust the # beams to process them with respect to a morton order */
  nbeams = (uint32_t)round_up_pow2(MMAX(beams_count[0], beams_count[1]));
  nbeams *= nbeams;

  /* Camera ray generation */
  FOR_EACH(mcode, 0, nbeams) {
    float* dir = ray_dirs + mcode * 3;
    float* dir_dx = ray_dirs_dx + mcode * 3;
    float* dir_dy = ray_dirs_dy + mcode * 3;
    float* range = ray_ranges + mcode * 2;
    float sample[2], sample_dx[2], sample_dy[2];

    /* Compute the beam indices in tile beam space */
    ibeam_x = morton2D_decode(mcode>>0);
    if(ibeam_x >= beams_count[0]) {
      f2(range, FLT_MAX, -FLT_MAX); /* <=> disable the ray */
      f3_splat(dir, 0.f); f3_splat(dir_dx, 0.f); f3_splat(dir_dy, 0.f);
      continue;
    }
    ibeam_y = morton2D_decode(mcode>>1);
    if(ibeam_y >= beams_count[1]) {
      f2(range, FLT_MAX, -FLT_MAX); /* <=> disable the ray */
      f3_splat(dir, 0.f); f3_splat(dir_dx, 0.f); f3_splat(dir_dy, 0.f);
      continue;
    }
    /* Transform beam indices in image space */
    ibeam_x = (uint16_t)(ibeam_x * beam_size[0] + tile_origin[0]);
    ibeam_y = (uint16_t)(ibeam_y * beam_size[1] + tile_origin[1]);
    /* Differentially sample the beam corner */
    sample[0] = ((float)ibeam_x) * pixel_size[0];
    sample[1] = ((float)ibeam_y) * pixel_size[1];
    sample_dx[0] = ((float)(ibeam_x + beam_size[0])) * pixel_size[0];
    sample_dx[1] = sample[1];
    sample_dy[0] = sample[0];
    sample_dy[1] = ((float)(ibeam_y + beam_size[1])) * pixel_size[1];
    camera_ray(cam, sample, ray_org, dir);
    camera_ray(cam, sample_dx, ray_org, dir_dx);
    camera_ray(cam, sample_dy, ray_org, dir_dy);
    f2_set(range, VXL_RAY_RANGE_DEFAULT);
  }

  /* Trace the beam reference rays */
  rt_state = state->rt_state;
  rt_state.lod_min = 0;
  VXL(scene_trace_drays
    (scene, &rt_state, nbeams, VXL_RAYS_SINGLE_ORIGIN, ray_org,
     ray_dirs, ray_dirs_dx, ray_dirs_dy, ray_ranges, hits));

  /* Compute the minimum hit distance in the beam */
  FOR_EACH(mcode, 0, nbeams) {
    ibeam_x = morton2D_decode(mcode>>0);
    if(ibeam_x >= beams_count[0] - 1) continue;
    ibeam_y = morton2D_decode(mcode>>1);
    if(ibeam_y >= beams_count[1] - 1) continue;
    beam_compute_min_depth(tile, ibeam_x, ibeam_y);
  }
}

static res_T
ray_trace_tile
  (struct tile* tile,
   struct vxl_scene* scene,
   struct vxr_camera* cam,
   struct vxr_framebuffer* fbuf,
   const unsigned tile_origin[2],
   const unsigned tile_size[2],
   const char trace_drays,
   const char beam_prepass,
   const struct vxr_draw_desc* state)
{
  float* ray_dirs, *ray_dirs_dx, *ray_dirs_dy, *ray_ranges;
  struct vxl_hit* hits;
  const unsigned beam_size[2] = { 8, 8 };
  uint32_t npixels;
  uint32_t mcode; /* Morton code */
  uint16_t ibeam_x, ibeam_y;
  uint16_t ipixel_x, ipixel_y;
  float ray_org[3];
  float pixel_size[2];
  unsigned char* pixels = NULL;
  ASSERT(tile && scene && cam && fbuf && tile_origin && tile_size && state);

  if(beam_prepass) {
    beam_trace(tile, scene, cam, fbuf, tile_origin, tile_size, beam_size,
      state);
  }

  pixel_size[0] = 1.f / (float)fbuf->size[0];
  pixel_size[1] = 1.f / (float)fbuf->size[1];

  /* Adjust the # pixels to process them with respect to a morton order */
  npixels = (uint32_t)round_up_pow2(MMAX(tile_size[0], tile_size[1]));
  npixels *= npixels;

  pixels = (unsigned char*)tile_get_pixels(tile);
  ray_dirs = tile_get_directions(tile);
  ray_dirs_dx = tile_get_directions_dx(tile);
  ray_dirs_dy = tile_get_directions_dy(tile);
  ray_ranges = tile_get_ranges(tile);
  hits = tile_get_hits(tile);

  /* Camera ray generation */
  FOR_EACH(mcode, 0, npixels) {
    float* dir = ray_dirs + mcode * 3;
    float* range = ray_ranges + mcode * 2;
    float sample[2];

    /* Compute the pixel indices in tile space */
    ipixel_x = morton2D_decode(mcode>>0);
    if(ipixel_x >= tile_size[0]) {
      f2(range, FLT_MAX, -FLT_MAX); /* disable the ray */
      continue;
    }
    ipixel_y = morton2D_decode(mcode>>1);
    if(ipixel_y >= tile_size[1]) {
      f2(range, FLT_MAX, -FLT_MAX); /* disable the ray */
      continue;
    }
    if(!beam_prepass) {
      f2(range, 0, FLT_MAX);
    } else {
      /* Setup the ray strating point from its corresponding beam min depth */
      ibeam_x = (uint16_t)(ipixel_x / beam_size[0]);
      ibeam_y = (uint16_t)(ipixel_y / beam_size[1]);
      f2(range, beam_get_min_depth(tile, ibeam_x, ibeam_y), FLT_MAX);
    }
    /* Transform pixel indices from tile space to image space */
    ipixel_x = (uint16_t)(ipixel_x + tile_origin[0]);
    ipixel_y = (uint16_t)(ipixel_y + tile_origin[1]);
    /* Sample the pixel center */
    sample[0] = ((float)ipixel_x + 0.5f) * pixel_size[0];
    sample[1] = ((float)ipixel_y + 0.5f) * pixel_size[1];
    camera_ray(cam, sample, ray_org, dir);
    /* Generate differential ray directions */
    if(trace_drays) {
      float* dir_dx = ray_dirs_dx + mcode * 3;
      float* dir_dy = ray_dirs_dy + mcode * 3;
      float sample_dx[2], sample_dy[2];

      sample_dx[0] = ((float)ipixel_x + 1.5f) * pixel_size[0];
      sample_dx[1] = sample[1];
      sample_dy[0] = sample[0];
      sample_dy[1] = ((float)ipixel_y + 1.5f) * pixel_size[1];
      camera_ray(cam, sample_dx, ray_org, dir_dx);
      camera_ray(cam, sample_dy, ray_org, dir_dy);
    }
  }
  /* Trace the rays through the image plane sample */
  if(trace_drays) {
    VXL(scene_trace_drays(scene, &state->rt_state, npixels,
      VXL_RAYS_SINGLE_ORIGIN, ray_org, ray_dirs, ray_dirs_dx, ray_dirs_dy,
      ray_ranges, hits));
  } else {
    VXL(scene_trace_rays(scene, &state->rt_state, npixels,
      VXL_RAYS_SINGLE_ORIGIN, ray_org, ray_dirs, ray_ranges,
      hits));
  }
  /* Shading */
  FOR_EACH(mcode, 0, npixels) {
    unsigned char* rgba;
    struct vxl_hit* hit = hits + mcode;
    /* Compute the pixel indices in tile space */
    ipixel_x = morton2D_decode(mcode>>0);
    if(ipixel_x >= tile_size[0]) continue;
    ipixel_y = morton2D_decode(mcode>>1);
    if(ipixel_y >= tile_size[1]) continue;
    /* Retrieve local pixel memory */
    rgba = pixels + (ipixel_y * tile_size[0] + ipixel_x) * 4 /* #channels */;
    /* Shading */
    hit_shade(hit, state->render_type, ray_dirs + mcode * 3, rgba);
  }
  return RES_OK;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
vxr_draw
  (struct vxr_device* dev,
   const struct vxr_draw_desc* draw_state,
   struct vxl_scene* scene,
   struct vxr_camera* cam,
   struct vxr_framebuffer* fbuf)
{
  struct tile* tiles;
  struct vxl_scene_desc scn_desc;
  const uint16_t tile_width = 32;
  const uint16_t tile_height = 32;
  uint16_t ntiles_x, ntiles_y;
  uint32_t ntiles;
  uint32_t mcode; /* Morton code of a tile */
  char trace_drays;
  char beam_prepass;
  ATOMIC res = RES_OK;

  if(!dev || !draw_state || !scene || !cam || !fbuf)
    return RES_BAD_ARG;

  VXL(scene_get_desc(scene, &scn_desc));

  beam_prepass = (draw_state->mask & VXR_DRAW_BEAM_PREPASS);

  /* Trace drays if the LOD min != LOD max and if LOD min is a valid SVO lvl */
  trace_drays = draw_state->rt_state.lod_min != draw_state->rt_state.lod_max
    && (1<<MMIN(draw_state->rt_state.lod_min, 16)) < scn_desc.definition;

  ntiles_x = (uint16_t)((fbuf->size[0] + (tile_width-1u)/*ceil*/)/tile_width);
  ntiles_y = (uint16_t)((fbuf->size[1] + (tile_height-1u)/*ceil*/)/tile_height);
  /* Adjust the # tiles to process them with respect to a morton order */
  ntiles = (uint32_t)round_up_pow2(MMAX(ntiles_x, ntiles_y));
  ntiles *= ntiles;

  tiles = darray_tile_data_get(&dev->tiles);

  omp_set_num_threads((int)dev->nthreads);
  #pragma omp parallel for schedule(dynamic)
  for(mcode = 0; mcode < ntiles; ++mcode) {
    const unsigned ithread = (unsigned)omp_get_thread_num();
    res_T res_local;

    if(ATOMIC_GET(&res) != RES_OK) continue;

    res_local = tile_resize(&tiles[ithread], tile_width, tile_height);
    if(res_local != RES_OK) {
      ATOMIC_SET(&res, res_local);
      continue;
    } else {
      unsigned tile_origin[2];
      unsigned tile_size[2];
      ASSERT((size_t)ithread < darray_tile_size_get(&dev->tiles));

      /* Origin of the tile to draw */
      tile_origin[0] = morton2D_decode(mcode>>0);
      if(tile_origin[0] >= ntiles_x) continue;
      tile_origin[1] = morton2D_decode(mcode>>1);
      if(tile_origin[1] >= ntiles_y) continue;
      tile_origin[0] = tile_origin[0] * tile_width;
      tile_origin[1] = tile_origin[1] * tile_height;
      tile_size[0] = (uint16_t)MMIN(tile_width, fbuf->size[0]-tile_origin[0]);
      tile_size[1] = (uint16_t)MMIN(tile_height, fbuf->size[1]-tile_origin[1]);
      res = ray_trace_tile(&tiles[ithread], scene, cam, fbuf, tile_origin,
        tile_size, trace_drays, beam_prepass, draw_state);

      if(res == RES_OK) {
        /* Flush tiles into the framebuffer memory */
        res = fbuf->writer(fbuf->ctx, tile_origin, tile_size, VXR_UBYTE_RGBA,
          darray_char_cdata_get(&tiles[ithread].pixels));
      }
    }
  }
  return (res_T)res;
}

