/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxr_c.h"
#include "vxr_device.h"
#include "vxr_framebuffer.h"

#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
framebuffer_release(ref_T* ref)
{
  struct vxr_framebuffer* buf = CONTAINER_OF(ref, struct vxr_framebuffer, ref);
  struct vxr_device* dev;
  ASSERT(ref);
  dev = buf->dev;
  MEM_RM(dev->allocator, buf);
  VXR(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
VXR_DECLARE_REF_FUNCS(framebuffer)

res_T
vxr_framebuffer_create(struct vxr_device* dev, struct vxr_framebuffer** buf_out)
{
  struct vxr_framebuffer* buf = NULL;
  res_T res = RES_OK;

  if(!dev || !buf_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  buf = MEM_CALLOC(dev->allocator, 1, sizeof(struct vxr_framebuffer));
  if(!buf) {
    res = RES_MEM_ERR;
    goto error;
  }
  VXR(device_ref_get(dev));
  buf->dev = dev;
  ref_init(&buf->ref);

exit:
  if(buf_out)
    *buf_out = buf;
  return res;
error:
  if(buf) {
    VXR(framebuffer_ref_put(buf));
    buf = NULL;
  }
  goto exit;
}

res_T
vxr_framebuffer_setup
  (struct vxr_framebuffer* buf,
   const unsigned size[2],
   const vxr_pixel_write_T writer,
   void* ctx)
{
  if(!buf || !size || !writer)
    return RES_BAD_ARG;

  buf->size[0] = size[0];
  buf->size[1] = size[1];
  buf->writer = writer;
  buf->ctx = ctx;
  return RES_OK;
}
