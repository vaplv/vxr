/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_vxr_utils.h"
#include <string.h>

int
main(int argc, char** argv)
{
  char block[8 /*#row*/ * 4/*#channels*/ * 8/*#column*/];
  struct vxr_device* dev;
  struct vxr_render_buffer* buf;
  struct mem_allocator allocator_proxy;
  struct vxr_image img;
  unsigned size[2] = { 72, 16 };
  unsigned org[2] = { 0, 0 };
  unsigned x, y, z;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxr_device_create
    (&allocator_proxy, NULL, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(vxr_render_buffer_create(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_create(dev, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_create(NULL, &buf) == RES_BAD_ARG);
  CHK(vxr_render_buffer_create(dev, &buf) == RES_OK);

  CHK(vxr_render_buffer_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_ref_get(buf) == RES_OK);
  CHK(vxr_render_buffer_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_ref_put(buf) == RES_OK);
  CHK(vxr_render_buffer_ref_put(buf) == RES_OK);

  CHK(vxr_render_buffer_create(dev, &buf) == RES_OK);
  CHK(vxr_render_buffer_setup(NULL, NULL, VXR_UBYTE_RGBA) == RES_BAD_ARG);
  CHK(vxr_render_buffer_setup(buf, NULL, VXR_UBYTE_RGBA) == RES_BAD_ARG);
  CHK(vxr_render_buffer_setup(NULL, size, VXR_UBYTE_RGBA) == RES_BAD_ARG);
  CHK(vxr_render_buffer_setup(buf, size, VXR_UBYTE_RGBA) == RES_OK);

  size[0] = 0;
  CHK(vxr_render_buffer_setup(buf, size, VXR_UBYTE_RGBA) == RES_BAD_ARG);
  size[0] = 72;
  size[1] = 0;
  CHK(vxr_render_buffer_setup(buf, size, VXR_UBYTE_RGBA) == RES_BAD_ARG);
  size[0] = 72;
  size[1] = 16;
  CHK(vxr_render_buffer_setup(buf, size, VXR_UBYTE_RGBA) == RES_OK);

  memset(block, 0xFF, sizeof(block));
  size[0] = size[1] = 8;
  CHK(vxr_render_buffer_write(NULL, NULL, NULL, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, NULL, NULL, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, org, NULL, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, org, NULL, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, NULL, size, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, NULL, size, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, org, size, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, NULL, NULL, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, NULL, NULL, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, org, NULL, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, org, NULL, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, NULL, size, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, NULL, size, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(NULL, org, size, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);

  org[0] = 65; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  org[0] = 64; org[1] = 9;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_BAD_ARG);
  org[0] = 64; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 70; org[1] = 14;
  size[0] = size[1] = 2;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);

  size[0] = size[1] = 8;
  FOR_EACH(y, 0, 2) {
    org[1] = y * 8;
    FOR_EACH(x, 0, 9) {
      org[0] = x * 8;
      CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
    }
  }

  CHK(vxr_render_buffer_image_get(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_image_get(buf, NULL) == RES_BAD_ARG);
  CHK(vxr_render_buffer_image_get(NULL, &img) == RES_BAD_ARG);
  CHK(vxr_render_buffer_image_get(buf, &img) == RES_OK);

  CHK(img.data != NULL);
  CHK(img.pitch == 72 * VXR_SIZEOF_PIXEL_FORMAT(VXR_UBYTE_RGBA));
  CHK(img.size[0] == 72);
  CHK(img.size[1] == 16);
  CHK(img.format == VXR_UBYTE_RGBA);

  FOR_EACH(y, 0, img.size[1]) {
    const char* row = img.data + y * img.pitch;
    FOR_EACH(x, 0, img.size[0]) {
      const char* pixel = row + x * VXR_SIZEOF_PIXEL_FORMAT(img.format);
      FOR_EACH( z, 0, VXR_SIZEOF_PIXEL_FORMAT(img.format)) {
        CHK((unsigned char)(pixel[z]) == 0xFF);
      }
    }
  }

  size[0] = size[1] = 8;

  FOR_EACH(y, 0, 8) {
    unsigned char* row = (unsigned char*)(block + y * 8 * 4);
    FOR_EACH(x, 0, 8) {
      unsigned char* pixel = row + x * 4;
      pixel[0] = (unsigned char)x;
      pixel[1] = (unsigned char)y;
      pixel[2] = 0;
      pixel[3] = 1;
    }
  }
  org[0] = 0; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 24; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 48; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 8; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 32; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 56; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);

  FOR_EACH(y, 0, 8) {
    unsigned char* row = (unsigned char*)(block + y * 8 * 4);
    FOR_EACH(x, 0, 8) {
      unsigned char* pixel = row + x * 4;
      pixel[0] = (unsigned char)y;
      pixel[1] = (unsigned char)x;
      pixel[2] = 127;
      pixel[3] = 128;
    }
  }
  org[0] = 8; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 32; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 56; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 16; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 40; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 64; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);

  FOR_EACH(y, 0, 8) {
    unsigned char* row = (unsigned char*)(block + y * 8 * 4);
    FOR_EACH(x, 0, 8) {
      unsigned char* pixel = row + x * 4;
      pixel[0] = (unsigned char)(255-x);
      pixel[1] = (unsigned char)(255-y);
      pixel[2] = 255;
      pixel[3] = 254;
    }
  }
  org[0] = 16; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 40; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 64; org[1] = 0;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 0; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 24; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);
  org[0] = 48; org[1] = 8;
  CHK(vxr_render_buffer_write(buf, org, size, VXR_UBYTE_RGBA, block) == RES_OK);

  FOR_EACH(y, 0, img.size[1]) {
    const unsigned char* row = (const unsigned char*)(img.data + y*img.pitch);
    FOR_EACH(x, 0, img.size[0]) {
      const unsigned char* pixel = row + x*VXR_SIZEOF_PIXEL_FORMAT(img.format);
      if((y< 8 && ((/*x>=0*/x<8)  || (x>=24 && x<32) || (x>=48 && x<56)))
      || (y>=8 && ((x>=8 && x<16) || (x>=32 && x<40) || (x>=56 && x<64)))) {
        CHK(pixel[0] == (unsigned char)(x % 8));
        CHK(pixel[1] == (unsigned char)(y % 8));
        CHK(pixel[2] == 0);
        CHK(pixel[3] == 1);
      } else if
      (  (y< 8 && ((x>=8  && x<16) || (x>=32 && x<40) || (x>=56 && x<64)))
      || (y>=8 && ((x>=16 && x<24) || (x>=40 && x<48) || (x>=64 && x<72)))) {
        CHK(pixel[0] == (unsigned char)(y % 8));
        CHK(pixel[1] == (unsigned char)(x % 8));
        CHK(pixel[2] == 127);
        CHK(pixel[3] == 128);
      } else if
      (  (y< 8 && ((x>=16 && x<24) || (x>=40 && x<48) || (x>=64 && x <72)))
      || (y>=8 && ((/*x>=0*/ x<8)  || (x>=24 && x<32) || (x>=48 && x<56)))) {
        CHK(pixel[0] == (unsigned char)(255 - (x % 8)));
        CHK(pixel[1] == (unsigned char)(255 - (y % 8)));
        CHK(pixel[2] == 255);
        CHK(pixel[3] == 254);
      } else { CHK(0 == 1); /* Unreachable code */ }
    }
  }

  CHK(vxr_render_buffer_ref_put(buf) == RES_OK);
  CHK(vxr_device_ref_put(dev) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);

  return 0;
}
