/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_vxr_utils.h"
#include <vx/vxl.h>

int
main(int argc, char** argv)
{
  struct vxr_device* dev;
  struct vxl_system* sys, *sys2;
  struct mem_allocator allocator_proxy;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);
  CHK(vxl_system_create(&allocator_proxy, 8.f, &sys) == RES_OK);

  CHK(vxr_device_create(NULL, NULL, VXR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(vxr_device_create
    (&allocator_proxy, NULL, VXR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(vxr_device_create
    (NULL, sys, VXR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(vxr_device_create
    (&allocator_proxy, sys, VXR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(vxr_device_create
    (NULL, NULL, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(vxr_device_get_system(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_device_get_system(dev, NULL) == RES_BAD_ARG);
  CHK(vxr_device_get_system(NULL, &sys2) == RES_BAD_ARG);
  CHK(vxr_device_get_system(dev, &sys2) == RES_OK);
  CHK(sys2 != sys);

  CHK(vxr_device_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxr_device_ref_get(dev) == RES_OK);
  CHK(vxr_device_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxr_device_ref_put(dev) == RES_OK);

  CHK(vxr_device_create
    (&allocator_proxy, NULL, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxr_device_create(NULL, sys, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxr_device_create
    (&allocator_proxy, sys, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(vxr_device_get_system(dev, &sys2) == RES_OK);
  CHK(sys2 == sys);
  
  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxl_system_ref_put(sys) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

