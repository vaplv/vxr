/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXR_C_H
#define VXR_C_H

#include "vxr.h"
#include <rsys/ref_count.h>

/* Helper macro declaring the body of the ref counting functions on Type. This
 * macro assumes that the struct vxl_##Type has an internal ref_T field named
 * `ref' and that a `void Type##_release(ref_T*)' function exists */
#define VXR_DECLARE_REF_FUNCS(Type)                                            \
  res_T                                                                        \
  vxr_ ## Type ## _ref_get(struct vxr_ ## Type* p)                             \
  {                                                                            \
    if(!p) return RES_BAD_ARG;                                                 \
    ref_get(&p->ref);                                                          \
    return RES_OK;                                                             \
  }                                                                            \
  res_T                                                                        \
  vxr_ ## Type ## _ref_put(struct vxr_ ## Type* p)                             \
  {                                                                            \
    if(!p) return RES_BAD_ARG;                                                 \
    ref_put(&p->ref, Type ## _release);                                        \
    return RES_OK;                                                             \
  }

#endif /* VXR_C_H */

