/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXR_DEVICE_H
#define VXR_DEVICE_H

#include "vxr.h"
#include "vxr_tile.h"

#include <rsys/dynamic_array_char.h>
#include <rsys/ref_count.h>
#include <rsys/mem_allocator.h>


/* Declare the darray_tile data structure */
#define DARRAY_NAME tile
#define DARRAY_DATA struct tile
#define DARRAY_FUNCTOR_INIT tile_init
#define DARRAY_FUNCTOR_RELEASE tile_release
#define DARRAY_FUNCTOR_COPY tile_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE tile_copy_and_release
#include <rsys/dynamic_array.h>

struct vxr_device {
  struct vxl_system* vxl_sys;
  ref_T ref;
  struct mem_allocator* allocator;

  unsigned nthreads;
  struct darray_tile tiles; /* Local rendering memory */
};

#endif /* VXR_DEVICE_H */

