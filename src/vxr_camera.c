/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxr_c.h"
#include "vxr_camera.h"
#include "vxr_device.h"

#include <rsys/float3.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
axis_compute
  (const float position[3],
   const float target[3],
   const float up[3],
   const float fov_x,
   const float rcp_proj_ratio,
   float axis_x[3],
   float axis_y[3],
   float axis_z[3])
{
  float img_plane_depth;
  ASSERT(position && target && up && fov_x > 0.f && rcp_proj_ratio > 0.f);
  ASSERT(axis_x && axis_y && axis_z);

  if(f3_normalize(axis_z, f3_sub(axis_z, target, position)) <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_x, f3_cross(axis_x, axis_z, up)) <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_y, f3_cross(axis_y, axis_z, axis_x)) <= 0.f)
    return RES_BAD_ARG;

  img_plane_depth = (float)(1.0/tan(fov_x*0.5));
  f3_mulf(axis_z, axis_z, img_plane_depth);
  f3_mulf(axis_y, axis_y, rcp_proj_ratio);
  return RES_OK;
}

static void
camera_release(ref_T* ref)
{
  struct vxr_camera* cam = CONTAINER_OF(ref, struct vxr_camera, ref);
  struct vxr_device* dev;
  ASSERT(ref);
  dev = cam->dev;
  MEM_RM(dev->allocator, cam);
  VXR(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
VXR_DECLARE_REF_FUNCS(camera)

res_T
vxr_camera_create(struct vxr_device* dev, struct vxr_camera** cam_out)
{
  struct vxr_camera* cam = NULL;
  res_T res = RES_OK;

  if(!dev || !cam_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  cam = MEM_CALLOC(dev->allocator, 1, sizeof(struct vxr_camera));
  if(!cam) {
    res = RES_MEM_ERR;
    goto error;
  }
  VXR(device_ref_get(dev));
  cam->dev = dev;
  ref_init(&cam->ref);
  VXR(camera_setup(cam, &VXR_CAMERA_DESC_DEFAULT));

exit:
  if(cam_out)
    *cam_out = cam;
  return res;
error:
  if(cam) {
    VXR(camera_ref_put(cam));
    cam = NULL;
  }
  goto exit;
}

res_T
vxr_camera_setup(struct vxr_camera* cam, const struct vxr_camera_desc* desc)
{
  float axis_x[3], axis_y[3], axis_z[3];
  float rcp_proj_ratio;
  res_T res;

  if(!cam || !desc || desc->fov_x <= 0.f
  || desc->fov_x > (float)MDEG2RAD(120.0) || desc->proj_ratio <= 0.f)
    return RES_BAD_ARG;

  rcp_proj_ratio = 1.f / desc->proj_ratio;
  res = axis_compute
    (desc->position, desc->target, desc->up, desc->fov_x, rcp_proj_ratio,
     axis_x, axis_y, axis_z);
  if(res != RES_OK)
    return res;
  f3_set(cam->axis_x, axis_x);
  f3_set(cam->axis_y, axis_y);
  f3_set(cam->axis_z, axis_z);
  f3_set(cam->position, desc->position);
  cam->fov_x = desc->fov_x;
  cam->rcp_proj_ratio = rcp_proj_ratio;
  return RES_OK;
}

res_T
vxr_camera_set_proj_ratio(struct vxr_camera* cam, const float proj_ratio)
{
  float axis_y[3] = {0.f, 0.f, 0.f};
  if(!cam || proj_ratio <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_y, cam->axis_y) <= 0.f)
    return RES_BAD_ARG;
  cam->rcp_proj_ratio = 1.f / proj_ratio;
  f3_mulf(cam->axis_y, axis_y, cam->rcp_proj_ratio);
  return RES_OK;
}

res_T
vxr_camera_get_proj_ratio(struct vxr_camera* cam, float* proj_ratio)
{
  if(!cam || !proj_ratio)
    return RES_BAD_ARG;
  *proj_ratio = 1.f / cam->rcp_proj_ratio;
  return RES_OK;
}

res_T
vxr_camera_set_fov(struct vxr_camera* cam, const float fov_x)
{
  float img_plane_depth;
  if(!cam || fov_x <= 0.f)
    return RES_BAD_ARG;
  img_plane_depth = (float)(1.0/tan(fov_x*0.5f));
  f3_normalize(cam->axis_z, cam->axis_z);
  f3_mulf(cam->axis_z, cam->axis_z, img_plane_depth);
  cam->fov_x = fov_x;
  return RES_OK;
}

res_T
vxr_camera_get_fov(const struct vxr_camera* cam, float* fov_x)
{
  if(!cam || !fov_x) return RES_BAD_ARG;
  *fov_x = cam->fov_x;
  return RES_OK;
}

res_T
vxr_camera_look_at
  (struct vxr_camera* cam,
   const float pos[3],
   const float target[3],
   const float up[3])
{
  float axis_x[3], axis_y[3], axis_z[3];
  res_T res;

  if(!cam || !pos || !target || !up)
    return RES_BAD_ARG;

  res = axis_compute
    (pos, target, up, cam->fov_x, cam->rcp_proj_ratio, axis_x, axis_y, axis_z);
  if(res != RES_OK)
    return res;
  f3_set(cam->axis_x, axis_x);
  f3_set(cam->axis_y, axis_y);
  f3_set(cam->axis_z, axis_z);
  f3_set(cam->position, pos);
  return RES_OK;
}

