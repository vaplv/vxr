/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_vxr_utils.h"
#include <rsys/float3.h>

int
main(int argc, char** argv)
{
  struct vxr_camera_desc desc = VXR_CAMERA_DESC_DEFAULT;
  struct vxr_device* dev;
  struct vxr_camera* cam;
  struct mem_allocator allocator_proxy;
  float f;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxr_device_create
    (&allocator_proxy, NULL, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(vxr_camera_create(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_create(dev, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_create(NULL, &cam) == RES_BAD_ARG);
  CHK(vxr_camera_create(dev, &cam) == RES_OK);

  CHK(vxr_camera_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxr_camera_ref_get(cam) == RES_OK);
  CHK(vxr_camera_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxr_camera_ref_put(cam) == RES_OK);
  CHK(vxr_camera_ref_put(cam) == RES_OK);

  CHK(vxr_camera_create(dev, &cam) == RES_OK);

  CHK(vxr_camera_setup(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_setup(cam, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_setup(NULL, &desc) == RES_BAD_ARG);
  CHK(vxr_camera_setup(cam, &desc) == RES_OK);

  desc.proj_ratio = 0.f;
  CHK(vxr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.proj_ratio = 1.f;
  CHK(vxr_camera_setup(cam, &desc) == RES_OK);
  f3_set(desc.position, desc.target);
  CHK(vxr_camera_setup(cam, &desc) == RES_BAD_ARG);
  f3_set(desc.position, VXR_CAMERA_DESC_DEFAULT.position);
  CHK(vxr_camera_setup(cam, &desc) == RES_OK);
  f3(desc.position, 0.f, 0.f, 0.f);
  f3(desc.target, 0.f, 1.f, 0.f);
  f3(desc.up, 0.f, 1.f, 0.f);
  CHK(vxr_camera_setup(cam, &desc) == RES_BAD_ARG);
  f3(desc.up, 0.f, 0.f, 1.f);
  CHK(vxr_camera_setup(cam, &desc) == RES_OK);
  desc.fov_x = 0.f;
  CHK(vxr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.fov_x = (float)PI;
  CHK(vxr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.fov_x = (float)PI*0.5f;
  CHK(vxr_camera_setup(cam, &desc) == RES_OK);

  CHK(vxr_camera_set_proj_ratio(NULL, 0.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_proj_ratio(cam, 0.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_proj_ratio(NULL, 4.f/3.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_proj_ratio(cam, 4.f/3.f) == RES_OK);
  CHK(vxr_camera_set_proj_ratio(cam, -4.f/3.f) == RES_BAD_ARG);

  CHK(vxr_camera_get_proj_ratio(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_get_proj_ratio(cam, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_get_proj_ratio(NULL, &f) == RES_BAD_ARG);
  CHK(vxr_camera_get_proj_ratio(cam, &f) == RES_OK);
  CHK(eq_eps(f, 4.f/3.f, 1.e-6f) == 1);

  CHK(vxr_camera_set_fov(NULL, 0.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_fov(cam, 0.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_fov(NULL, (float)PI/4.f) == RES_BAD_ARG);
  CHK(vxr_camera_set_fov(cam, (float)PI/4.f) == RES_OK);
  CHK(vxr_camera_set_fov(cam, -(float)PI/4.f) == RES_BAD_ARG);

  CHK(vxr_camera_get_fov(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_get_fov(cam, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_get_fov(NULL, &f) == RES_BAD_ARG);
  CHK(vxr_camera_get_fov(cam, &f) == RES_OK);
  CHK(f == (float)PI/4.f);

  CHK(vxr_camera_look_at(NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, desc.position, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, desc.position, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, NULL, desc.target, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, NULL, desc.target, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, desc.position, desc.target, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, desc.position, desc.target, NULL) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, NULL, NULL, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, NULL, NULL, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, desc.position, NULL, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, desc.position, NULL, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, NULL, desc.target, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, NULL, desc.target, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(NULL, desc.position, desc.target, desc.up) == RES_BAD_ARG);
  CHK(vxr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_OK);

  f3(desc.position, 0.f, 0.f, 0.f);
  f3(desc.target, 0.f, 1.f, 0.f);
  f3(desc.up, 0.f, 1.f, 0.f);
  CHK(vxr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_BAD_ARG);
  f3(desc.up, 0.f, 0.f, 1.f);
  CHK(vxr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_OK);
  f3(desc.target, 0.f, 0.f, 0.f);
  CHK(vxr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_BAD_ARG);

  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxr_camera_ref_put(cam) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

