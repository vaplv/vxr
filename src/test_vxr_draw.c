/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_vxr_utils.h"
#include <rsys/float3.h>
#include <rsys/image.h>
#include <vx/vxl.h>

struct mesh {
  const float* verts;
  size_t nverts;
  const uint32_t* ids;
  size_t nids;
};

static void
get_ids(const uint64_t itri, uint64_t ids[3], void* data)
{
  const uint64_t id = itri * 3;
  struct mesh* msh = data;
  CHK(id + 2 < msh->nids);
  ids[0] = msh->ids[id + 0];
  ids[1] = msh->ids[id + 1];
  ids[2] = msh->ids[id + 2];
}

static void
get_pos(const uint64_t ivert, float verts[3], void* data)
{
  struct mesh* msh = data;
  CHK(ivert < msh->nverts);
  f3_set(verts, msh->verts + (ivert*3));
}

static void
triangle_get_normal
  (const uint64_t itri,
   const float uvw[3],
   float normal[3],
   void* data)
{
  uint64_t ids[3];
  float v0[3], v1[3], v2[3], e0[3], e1[3];
  (void)uvw;
  get_ids(itri, ids, data);
  get_pos(ids[0], v0, data);
  get_pos(ids[1], v1, data);
  get_pos(ids[2], v2, data);
  f3_sub(e0, v1, v0);
  f3_sub(e1, v2, v0);
  f3_normalize(normal, f3_cross(normal, e0, e1));
}

static void
triangle_get_color
  (const uint64_t itri,
   const float uvw[3],
   unsigned char color[3],
   void* data)
{
  (void)itri, (void)data;
  color[0] = (unsigned char)(uvw[0] * 255.f);
  color[1] = (unsigned char)(uvw[1] * 255.f);
  color[2] = (unsigned char)(uvw[2] * 255.f);
}

static void
scene_create(struct vxl_system* sys, struct vxl_scene** scn)
{
  const float verts[] = {
    /* Box */
    552.f, 0.f,   0.f,
    0.f,   0.f,   0.f,
    0.f,   559.f, 0.f,
    552.f, 559.f, 0.f,
    552.f, 0.f,   548.f,
    0.f,   0.f,   548.f,
    0.f,   559.f, 548.f,
    552.f, 559.f, 548.f,
    /* Short block */
    130.f, 65.f,  0.f,
    82.f,  225.f, 0.f,
    240.f, 272.f, 0.f,
    290.f, 114.f, 0.f,
    130.f, 65.f,  165.f,
    82.f,  225.f, 165.f,
    240.f, 272.f, 165.f,
    290.f, 114.f, 165.f,
    /* Tall block */
    423.0f, 247.0f, 0.f,
    265.0f, 296.0f, 0.f,
    314.0f, 456.0f, 0.f,
    472.0f, 406.0f, 0.f,
    423.0f, 247.0f, 330.f,
    265.0f, 296.0f, 330.f,
    314.0f, 456.0f, 330.f,
    472.0f, 406.0f, 330.f
  };
  const size_t nverts = sizeof(verts) / (sizeof(float)*3);
  const uint32_t ids[] = {
    /* Box */
    0, 1, 2, 2, 3, 0,
    4, 5, 6, 6, 7, 4,
    1, 2, 6, 6, 5, 1,
    0, 3, 7, 7, 4, 0,
    2, 3, 7, 7, 6, 2,
    /* Short block */
    12, 13, 14, 14, 15, 12,
    9, 10, 14, 14, 13, 9,
    8, 11, 15, 15, 12, 8,
    10, 11, 15, 15, 14, 10,
    8, 9, 13, 13, 12, 8,
    /* Tall block */
    20, 21, 22, 22, 23, 20,
    17, 18, 22, 22, 21, 17,
    16, 19, 23, 23, 20, 16,
    18, 19, 23, 23, 22, 18,
    16, 17, 21, 21, 20, 16
  };
  const size_t nids = sizeof(ids)/sizeof(uint32_t);

  struct vxl_mesh_desc mesh_desc = VXL_MESH_DESC_NULL;

  struct mesh msh;

  CHK(scn != NULL);
  CHK(vxl_scene_create(sys, scn) == RES_OK);

  /* Cornell box */
  msh.verts = verts;
  msh.nverts = nverts;
  msh.ids = ids;
  msh.nids = nids;
  mesh_desc.ntris = nids / 3;
  mesh_desc.get_ids = get_ids;
  mesh_desc.get_pos = get_pos;
  mesh_desc.get_normal = triangle_get_normal;
  mesh_desc.get_color = triangle_get_color;
  mesh_desc.data = &msh;
  CHK(vxl_scene_setup
    (*scn, &mesh_desc, 1, 64, VXL_MEM_INCORE, NULL, NULL) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct vxr_device* dev;
  struct vxr_camera* cam;
  struct vxr_framebuffer* fbuf;
  struct vxr_render_buffer* rbuf;
  struct vxl_system* sys;
  struct vxl_scene* scn;
  struct vxr_camera_desc cam_desc = VXR_CAMERA_DESC_DEFAULT;
  struct vxr_draw_desc draw_desc = VXR_DRAW_DESC_DEFAULT;
  struct vxr_image img;
  struct mem_allocator allocator_proxy;
  const unsigned img_size[2] = { 512, 512 };
  unsigned x, y;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxl_system_create(&allocator_proxy, VXL_NTHREADS_DEFAULT, &sys) == RES_OK);
  scene_create(sys, &scn);

  CHK(vxr_device_create
    (&allocator_proxy, sys, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(vxr_camera_create(dev, &cam) == RES_OK);
  f3(cam_desc.position, 278.f, -1000.f, 273.f);
  f3(cam_desc.target, 278.f, 0.f, 273.f);
  f3(cam_desc.up, 0.f, 0.f, 1.f);
  cam_desc.proj_ratio  = (float)img_size[0]/(float)img_size[1];
  cam_desc.fov_x = (float)PI*0.25f;
  CHK(vxr_camera_setup(cam, &cam_desc) == RES_OK);

  CHK(vxr_render_buffer_create(dev, &rbuf) == RES_OK);
  CHK(vxr_render_buffer_setup(rbuf, img_size, VXR_UBYTE_RGBA) == RES_OK);
  CHK(vxr_framebuffer_create(dev, &fbuf) == RES_OK);
  CHK(vxr_framebuffer_setup
    (fbuf, img_size, vxr_render_buffer_write, rbuf) == RES_OK);

  CHK(vxr_draw(NULL, &draw_desc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, scn, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, scn, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, NULL, cam, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, NULL, cam, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, scn, cam, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, scn, cam, NULL) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, NULL, NULL, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, NULL, NULL, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, scn, NULL, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, scn, NULL, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, NULL, cam, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, NULL, cam, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(NULL, &draw_desc, scn, cam, fbuf) == RES_BAD_ARG);
  CHK(vxr_draw(dev, &draw_desc, scn, cam, fbuf) == RES_OK);
  CHK(vxr_draw(dev, NULL, scn, cam, fbuf) == RES_BAD_ARG);
  draw_desc.mask = 0;
  CHK(vxr_draw(dev, &draw_desc, scn, cam, fbuf) == RES_OK);

  CHK(vxr_render_buffer_image_get(rbuf, &img) == RES_OK);
  CHK(img.size[0] == img_size[0]);
  CHK(img.size[1] == img_size[1]);
  CHK(img.format == VXR_UBYTE_RGBA);
  CHK(img.pitch == img_size[0] * VXR_SIZEOF_PIXEL_FORMAT(img.format));

  printf("P3 %u %u %u\n", img_size[0], img_size[1], 255);
  FOR_EACH(y, 0, img.size[1]) {
    char* row = img.data + img.pitch * y;
    FOR_EACH(x, 0, img.size[0]) {
      uint8_t* pixel = (uint8_t*)(row + x * VXR_SIZEOF_PIXEL_FORMAT(img.format));
      printf("%u %u %u\n", SPLIT3(pixel));
    }
  }

  CHK(vxr_device_ref_put(dev) == RES_OK);
  CHK(vxr_camera_ref_put(cam) == RES_OK);
  CHK(vxr_framebuffer_ref_put(fbuf) == RES_OK);
  CHK(vxr_render_buffer_ref_put(rbuf) == RES_OK);
  CHK(vxl_system_ref_put(sys) == RES_OK);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

