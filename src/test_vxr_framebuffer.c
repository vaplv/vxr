/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_vxr_utils.h"

int
main(int argc, char** argv)
{
  struct vxr_device* dev;
  struct vxr_framebuffer* buf;
  struct mem_allocator allocator_proxy;
  const unsigned size[2] = { 320, 240 };
  struct vxr_render_buffer* rbuf;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxr_device_create
    (&allocator_proxy, NULL, VXR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(vxr_framebuffer_create(NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_create(dev, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_create(NULL, &buf) == RES_BAD_ARG);
  CHK(vxr_framebuffer_create(dev, &buf) == RES_OK);

  CHK(vxr_framebuffer_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_ref_get(buf) == RES_OK);
  CHK(vxr_framebuffer_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_ref_put(buf) == RES_OK);
  CHK(vxr_framebuffer_ref_put(buf) == RES_OK);

  CHK(vxr_framebuffer_create(dev, &buf) == RES_OK);

  CHK(vxr_framebuffer_setup(NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(buf, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(NULL, size, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(buf, size, NULL, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(NULL, NULL, vxr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(buf, NULL, vxr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(NULL, size, vxr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(vxr_framebuffer_setup(buf, size, vxr_render_buffer_write, NULL) == RES_OK);

  CHK(vxr_render_buffer_create(dev, &rbuf) == RES_OK);
  CHK(vxr_render_buffer_setup(rbuf, size, VXR_UBYTE_RGBA) == RES_OK);
  CHK(vxr_framebuffer_setup(buf, size, vxr_render_buffer_write, rbuf) == RES_OK);

  CHK(vxr_render_buffer_ref_put(rbuf) == RES_OK);
  CHK(vxr_framebuffer_ref_put(buf) == RES_OK);
  CHK(vxr_device_ref_put(dev) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

