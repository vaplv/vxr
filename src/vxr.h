/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXR_H
#define VXR_H

#include <rsys/math.h>
#include <vx/vxl.h>

#ifdef VXR_SHARED_BUILD
  #define VXR_API extern EXPORT_SYM
#else
  #define VXR_API extern IMPORT_SYM
#endif

#ifndef NDEBUG
  #define VXR(Func) ASSERT(vxr_##Func == RES_OK)
#else
  #define VXR(Func) vxr_##Func
#endif

#define VXR_NTHREADS_DEFAULT ~0u

struct mem_allocator;

/*******************************************************************************
 * Public types and functions
 ******************************************************************************/
enum vxr_pixel_format { /* List of supported pixel format */
  VXR_UBYTE_RGBA
};

static INLINE size_t
VXR_SIZEOF_PIXEL_FORMAT(const enum vxr_pixel_format format)
{
  size_t BPP = 0;
  switch(format) {
    case VXR_UBYTE_RGBA: BPP = sizeof(unsigned char[4]); break;
    default: FATAL("Unreachable code\n"); break;
  }
  return BPP;
}

struct vxr_camera_desc { /* Describe a pinhole camera */
  float position[3];
  float target[3];
  float up[3];
  float proj_ratio; /* width / height */
  float fov_x;
};

static const struct vxr_camera_desc VXR_CAMERA_DESC_DEFAULT = {
  { 278.f, -800.f, 273.f }, /* Position */
  { 278.f, 0.f, 273.f }, /* Target */
  { 0.f, 0.f, 1.f }, /* Up */
  4.f / 3.f, /* Projection ration */
  (float)PI * 0.5f /* Field of View */
};

typedef res_T
(*vxr_pixel_write_T)
  (void* context, /* User defined data */
   const unsigned origin[2], /* 2D coordinates of the first pixel to write */
   const unsigned size[2], /* Number of pixels in X and Y to write */
   const enum vxr_pixel_format format, /* Format of source pixels */
   const void* pixels); /* List of row ordered pixels */

struct vxr_image {
  char* data; /* Row ordered pixels */
  size_t pitch; /* Size in bytes between 2 consecutive rows */
  unsigned size[2]; /* Image definition */
  enum vxr_pixel_format format; /* Pixel format of the data */
};

static const struct vxr_image VXR_IMAGE_NULL =
{ NULL, 0, {0, 0}, VXR_UBYTE_RGBA };

enum vxr_render_type {
  VXR_RENDER_LEGACY,
  VXR_RENDER_CUBIC, /* Use the voxel face normal rather than the voxel normal */
  VXR_RENDER_COLOR, /* Display raw voxel color */
  VXR_RENDER_NORMAL, /* Display voxel normal */
  VXR_RENDER_TYPES_COUNT__
};

enum vxr_draw_flag {
  VXR_DRAW_BEAM_PREPASS = BIT(0) /* Improve the SVO ray casting performances */
};

struct vxr_draw_desc {
  struct vxl_rt_desc rt_state;
  enum vxr_render_type render_type;
  int mask; /* Combination of vxr_draw_flag */
};

static const struct vxr_draw_desc VXR_DRAW_DESC_DEFAULT =
{ VXL_RT_DESC_DEFAULT__, VXR_RENDER_LEGACY, 0 };

/*******************************************************************************
 * A device is the factory of all other resources. In addition, it performs the
 * rendering operations
 ******************************************************************************/
struct vxr_device;

BEGIN_DECLS

VXR_API res_T
vxr_device_create
  (struct mem_allocator* allocator, /* May be NULL <=> Use default allocator */
   struct vxl_system* vxl, /* May be NULL <=> Internally create a vxl system */
   const unsigned nthreads_hint, /* Hint on the number of threads to use */
   struct vxr_device** dev);

VXR_API res_T
vxr_device_get_system
  (struct vxr_device* dev,
   struct vxl_system** sys);

VXR_API res_T
vxr_device_ref_get
  (struct vxr_device* dev);

VXR_API res_T
vxr_device_ref_put
  (struct vxr_device* dev);

END_DECLS

/*******************************************************************************
 * A camera stores the rendering point of view
 ******************************************************************************/
struct vxr_camera;

BEGIN_DECLS

VXR_API res_T
vxr_camera_create
  (struct vxr_device* dev,
   struct vxr_camera** cam);

VXR_API res_T
vxr_camera_ref_get
  (struct vxr_camera* cam);

VXR_API res_T
vxr_camera_ref_put
  (struct vxr_camera* cam);

VXR_API res_T
vxr_camera_setup
  (struct vxr_camera* cam,
   const struct vxr_camera_desc* desc);

VXR_API res_T
vxr_camera_set_proj_ratio
  (struct vxr_camera* cam,
   const float proj_ratio);

VXR_API res_T
vxr_camera_get_proj_ratio
  (struct vxr_camera* cam,
   float* proj_ratio);

VXR_API res_T
vxr_camera_set_fov
  (struct vxr_camera* cam,
   const float fov_x); /* in radian */

VXR_API res_T
vxr_camera_get_fov
  (const struct vxr_camera* cam,
   float* fov_x); /* in radian */

VXR_API res_T
vxr_camera_look_at
  (struct vxr_camera* cam,
   const float position[3],
   const float target[3],
   const float up[3]);

END_DECLS

/*******************************************************************************
 * A framebuffer encapsulates the rendering memory. Note that the image
 * buffer management is left to the the host application through the
 * vxr_pixel_write_T functor
 ******************************************************************************/
struct vxr_framebuffer;

BEGIN_DECLS

VXR_API res_T
vxr_framebuffer_create
  (struct vxr_device* dev,
   struct vxr_framebuffer** buf);

VXR_API res_T
vxr_framebuffer_ref_get
  (struct vxr_framebuffer* buf);

VXR_API res_T
vxr_framebuffer_ref_put
  (struct vxr_framebuffer* buf);

VXR_API res_T
vxr_framebuffer_setup
  (struct vxr_framebuffer* buf,
   const unsigned size[2],
   const vxr_pixel_write_T writer,
   void* context); /* Host data sent as the first parameter of the writer */

END_DECLS

/*******************************************************************************
 * A render buffer is an helper data structure that manages output image buffer
 * memory space
 ******************************************************************************/
struct vxr_render_buffer;

BEGIN_DECLS

VXR_API res_T
vxr_render_buffer_create
  (struct vxr_device* dev,
   struct vxr_render_buffer** buf);

VXR_API res_T
vxr_render_buffer_ref_get
  (struct vxr_render_buffer* buf);

VXR_API res_T
vxr_render_buffer_ref_put
  (struct vxr_render_buffer* buf);

VXR_API res_T
vxr_render_buffer_setup
  (struct vxr_render_buffer* buf,
   const unsigned size[2],
   const enum vxr_pixel_format format);

/* Retrieve the render buffer image data. The returned image remains valid
 * until the next `vxr_render_buffer_setup' invocation */
VXR_API res_T
vxr_render_buffer_image_get
  (struct vxr_render_buffer* buf,
   struct vxr_image* img);

/* Helper function that match the `vxr_pixel_write_T' functor type */
VXR_API res_T
vxr_render_buffer_write
  (void* render_buffer,
   const unsigned origin[2],
   const unsigned size[2],
   const enum vxr_pixel_format format,
   const void* pixels);

END_DECLS

/*******************************************************************************
 * Draw functions
 ******************************************************************************/
BEGIN_DECLS

/* Invoke the draw of a scene from a given point of view into a provided
 * framebuffer */
VXR_API res_T
vxr_draw
  (struct vxr_device* dev,
   const struct vxr_draw_desc* draw_state,
   struct vxl_scene* scn,
   struct vxr_camera* cam,
   struct vxr_framebuffer* buf);

END_DECLS

#endif /* VXR_H */

